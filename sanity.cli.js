import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'ed6dl41d',
    dataset: 'production'
  }
})
